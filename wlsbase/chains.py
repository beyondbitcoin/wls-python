default_prefix = "WLS"

known_chains = {
    "WLS": {
        "chain_id": "de999ada2ff7ed3d3d580381f229b40b5a0261aec48eb830e540080817b72866",
        "prefix": "WLS",
        "steem_symbol": "WLS",
        "vests_symbol": "VESTS",
    },
    "TEST": {
        "chain_id": "9afbce9f2416520733bacb370315d32b6b2c43d6097576df1c1222859d91eecc",
        "prefix": "TST",
        "steem_symbol":"TESTS",
        "vests_symbol": "VESTS",
    },
}
